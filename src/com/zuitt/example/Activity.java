package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args){
        String[] fruits = {"apple","avocado","banana","kiwi","orange"};
        System.out.println("Fruits in stock: " + Arrays.toString(fruits));

        Scanner newFruit = new Scanner(System.in);
        System.out.println("Which fruit would you like to get the index of? ");
        String fruitName = newFruit.nextLine();
        int fruitIndex = Arrays.asList(fruits).indexOf(fruitName);
        System.out.println("The index of " + fruitName + " is: " + fruitIndex);

        ArrayList<String> friends = new ArrayList<>(Arrays.asList("John", "Paul", "Grace", "Jerome"));
        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put("toothpaste",15);
        inventory.put("toothbrush",20);
        inventory.put("soap",12);
        System.out.println("Our current inventory consists of: \n" + inventory);

    }
}
